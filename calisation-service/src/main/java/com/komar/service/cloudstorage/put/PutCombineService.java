package com.komar.service.cloudstorage.put;

import com.komar.domain.cloudstorage.resource.transfer.put.PutResultTO;
import com.komar.domain.resource.transfer.ClipCombination;

public interface PutCombineService {
	
	PutResultTO putCombine(ClipCombination clipCombination) throws PutException;

}
