package com.komar.service.cloudstorage.put;

import com.komar.domain.cloudstorage.resource.transfer.put.PutResultTO;
import com.komar.domain.resource.transfer.Edit;

public interface PutEditService<T>{

    PutResultTO putEdit(T file, Edit edit) throws PutException;
}
