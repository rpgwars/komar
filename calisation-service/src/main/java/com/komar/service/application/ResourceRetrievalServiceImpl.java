package com.komar.service.application;


import com.komar.domain.cloudstorage.resource.Resource;
import com.komar.domain.resource.transfer.ResourceTO;
import com.komar.repository.ResourceDAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ResourceRetrievalServiceImpl implements ResourceRetrievalService{

	private static final int resourcePageRatio = 3;
	
	@Autowired
	private ResourceDAO resourceDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<ResourceTO> getResources(int pageNr) {
		List<Resource> resources = resourceDao.getResources(resourcePageRatio, pageNr);
		List<ResourceTO> resourceTOs = resources.stream().map(resource -> resource.toTO()).collect(Collectors.toList());
		return resourceTOs;
	}

	@Override
	@Transactional(readOnly = true)
	public int getPagesNr() {
		Long resourceNr = resourceDao.getResourceNr();
		return (int)Math.ceil(resourceNr / resourcePageRatio);
	}
	
	@Override
	@Transactional(readOnly = true)
	public ResourceTO getResource(String key) {
		Resource resource = resourceDao.getResource(key);
		return resource.toTO();
	}

	public ResourceDAO getResourceDao() {
		return resourceDao;
	}

	public void setResourceDao(ResourceDAO resourceDao) {
		this.resourceDao = resourceDao;
	}
}
