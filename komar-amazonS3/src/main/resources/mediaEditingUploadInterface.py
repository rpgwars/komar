import os
import sys
import moviepyInterface
import botoInterface
import json
import youtubedlInterface


def getVideoClip(filePath, start, end, withAudio, bucketName, profileName):
    if(start == "-1"):
        start = None
    if(end == "-1"):
        end = None
    videoClipFile = ""
    try:
        videoClipFile = moviepyInterface.makeVideoClip(filePath, start, end, withAudio)
        key = filePath.split('/')[-1:][0]
        clipLink = botoInterface.putClip(profileName, bucketName, videoClipFile, key)
        print clipLink
        return clipLink
    finally:
        os.remove(filePath)
        os.remove(videoClipFile)
    

def getAudioClip(filePath, start, end, bucketName, profileName):
    if(start == "-1"):
        start = None
    if(end == "-1"):
        end = None
    audioClipFile = ""
    try:
        audioClipFile = moviepyInterface.makeAudioClip(filePath, start, end)
        key = audioClipFile.split('/clip_')[-1:][0]
        clipLink = botoInterface.putClip(profileName, bucketName, audioClipFile, key)
        print clipLink
        return clipLink
    finally:
        os.remove(filePath)
        os.remove(audioClipFile)

def prepareClipsForCombination(dataFile, fileFolder, bucketName, profileName):
    data = json.load(open(fileFolder + dataFile))
    videoClips = data["videoClips"]
    selectedAudioClips = data["selectedAudioClips"]
    videoClipFiles = []
    selectedAudioClipFiles = []
    filesWithAudio = []
    for videoClip in videoClips:
	fileName = botoInterface.getClip(profileName, bucketName, videoClip["url"].split('/')[-1:][0], fileFolder)
	if(fileName == None):
	    cleanupClips(videoClipFiles, selectedAudioClipFiles)
	    return None
	videoClipFiles.append(fileFolder + fileName)
	filesWithAudio.append(videoClip["withAudio"])

    for audioClips in selectedAudioClips:
	audioClipFiles = []
	selectedAudioClipFiles.append(audioClipFiles)
	for audioClip in audioClips:
	    fileName = botoInterface.getClip(profileName, bucketName, audioClip["url"].split('/')[-1:][0], fileFolder)
	    if(fileName == None):
	        cleanupClips(videoClipFiles, selectedAudioClipFiles)
		return None
	    audioClipFiles.append(fileFolder + fileName)

    return (selectedAudioClipFiles, videoClipFiles, filesWithAudio)
	

def cleanupClips(selectedAudioClipFiles, videoClipFiles):
    for videoClipFile in videoClipFiles:
	os.remove(videoClipFile)
    for audioClipFiles in selectedAudioClipFiles:
	for audioClipFile in audioClipFiles:
	    os.remove(audioClipFile)
	
def getClipCombination(dataFile, fileFolder, bucketName, profileName):
    clipCombination = prepareClipsForCombination(dataFile, fileFolder, bucketName, profileName)
    os.remove(fileFolder+dataFile)
    if(clipCombination != None):
	moviepyInterface.concatenateClips(clipCombination[0], clipCombination[1], clipCombination[2], fileFolder + dataFile + ".mp4")
	cleanupClips(clipCombination[0], clipCombination[1])
	try:
            clipLink = botoInterface.putClip(profileName, bucketName, fileFolder + dataFile + ".mp4", dataFile + ".mp4")
	    print clipLink
	    return clipLink
	finally:
	    os.remove(fileFolder + dataFile + ".mp4")

    raise Exception()

def getYouTubeVideoClip(youTubeLink, key, fileFolder, start, end, withAudio, extractAudio, bucketName, profileName):
    filePath = youtubedlInterface.getYouTubeVideoClip(youTubeLink, key, fileFolder, extractAudio)
    if(extractAudio):
        getAudioClip(filePath, start, end, bucketName, profileName)
    else:
        getVideoClip(filePath, start, end, withAudio, bucketName, profileName)

if sys.argv[1] == 'youtube':
    getYouTubeVideoClip(sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5], sys.argv[6], sys.argv[7] in ['true'], sys.argv[8] in ['true'], sys.argv[9], sys.argv[10])
elif len(sys.argv) == 5:
    getClipCombination(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
elif len(sys.argv) == 6:
    getAudioClip(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5])
else:
    getVideoClip(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4] in ['true'], sys.argv[5], sys.argv[6])

#getVideoClip("MySample", 30, 40, True, "komar-sandbox")
#getAudioClip("MyAudioSample", 30, 40, "komar-sandbox")
#getYouTubeVideoClip('youtube', 'https://www.youtube.com/watch?v=Uxg5W5wKvLE', 'yKey', '/home/damian/komar/python/','5','10','true','komar-sandbox','KOMAR_S3')
