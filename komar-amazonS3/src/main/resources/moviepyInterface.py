from moviepy.editor import *
import ntpath
import re

def getSubclip(media, start, end):
    mediaClip = None
    if(start == None and end == None):
        mediaClip = media
    else:
        if(start == None):
            start = 0
        if(end == None):
            end = media.duration
        mediaClip = media.subclip(float(start), float(end))
    return mediaClip

def makeVideoClip(filePath, start, end, withAudio):
    originalVideo = VideoFileClip(filePath)
    video = getSubclip(originalVideo, start, end)
    clipFile = ntpath.dirname(filePath) + "/clip_" + ntpath.basename(filePath)
    video.write_videofile(clipFile, audio=withAudio) # Many options...
    return clipFile

def makeAudioClip(filePath, start, end):
    originalAudio = AudioFileClip(filePath)
    audio = getSubclip(originalAudio, start, end)
    clipFile = ntpath.dirname(filePath) + "/clip_" + ntpath.basename(filePath)
    clipFile = re.sub('\.[a-zA-Z0-9]+$','.mp3',clipFile)
    audio.write_audiofile(clipFile)
    return clipFile

def concatenateAudioClips(videoClipDuration, audioClipFiles):
    audioClips = []
    audioClipsDuration = 0
    audioClipsDurationOverflow = False
    for audioClipFile in audioClipFiles:
        audioClip = AudioFileClip(audioClipFile)
	audioClips.append(audioClip)
	audioClipsDuration += audioClip.duration
	if(audioClipsDuration > videoClipDuration):
	    audioClipsDurationOverflow = True
	    break

    if(audioClipsDuration == 0):
	return None

    if(audioClipsDurationOverflow):
	lastAudioClip = audioClips[-1:][0]
	lastAudioClipDuration =  videoClipDuration - audioClipsDuration + lastAudioClip.duration
	lastAudioClip = lastAudioClip.set_duration(lastAudioClipDuration)
	audioClips[len(audioClips) - 1] = lastAudioClip

    return concatenate_audioclips(audioClips)

def concatenateClips(audioFiles, videoFiles, filesWithAudio, fileName):

    clips = []
    videoIndex = 0;
    for videoFile in videoFiles:
        videoClip = VideoFileClip(videoFiles[videoIndex])
	if(not filesWithAudio[videoIndex]):
            audioTrack = concatenateAudioClips(videoClip.duration, audioFiles[videoIndex])
	    if(audioTrack != None):
	        clips.append(videoClip.set_audio(audioTrack))
	    else:
	        clips.append(videoClip)
	else:
	    clips.append(videoClip)

        videoIndex = videoIndex + 1

    video = concatenate_videoclips(clips, method="compose")
    video.write_videofile(fileName, fps=24)


#concatenateClips([["10sec.mp3","8sec.mp3"],[],["2sec.mp3","4sec.mp3"]], ["10secNoAudio.mp4","10secAudio.mp4","20secNoAudio.mp4"], [False, True, False], 'full.mp4')
#makeVideoClip("/home/damian/media/video.mp4",30,40,True)
#makeAudioClip("/home/damian/media/audio.mp3",0,10)
#makeAudioClip("/home/damian/media/audio.mp3",10,18)
#makeAudioClip("/home/damian/media/audio.mp3",18,22)
#makeAudioClip("/home/damian/media/audio.mp3",22,24)
#concatenateClips(["a.mp3",""],["v.mp4","small.ogv"])


