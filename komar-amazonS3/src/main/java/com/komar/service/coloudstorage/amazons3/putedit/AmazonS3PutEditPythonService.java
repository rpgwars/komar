package com.komar.service.coloudstorage.amazons3.putedit;

import com.komar.domain.cloudstorage.resource.transfer.put.PutResultTO;
import com.komar.domain.resource.transfer.UploadedFile;
import com.komar.service.cloudstorage.put.PutEditService;
import com.komar.service.cloudstorage.put.PutException;
import com.komar.service.coloudstorage.amazons3.put.AmazonS3AbstractPutPythonService;
import com.komar.service.coloudstorage.amazons3.put.AmazonS3PutResultTO;
import com.komar.domain.resource.transfer.Edit;
import org.springframework.stereotype.Service;

@Service
public class AmazonS3PutEditPythonService extends AmazonS3AbstractPutPythonService implements PutEditService<UploadedFile> {

    @Override
    public PutResultTO putEdit(UploadedFile file, Edit edit) throws PutException {
        String key = keyGenerator.getKey();
        saveFile(file, key);
        String link = executePythonScripts(file, edit, key);
        return new AmazonS3PutResultTO(bucketName, key, link, file.getFile().getContentType());
    }

    private String executePythonScripts(UploadedFile file, Edit edit, String key) throws PutException {
            String command;
            String start = edit.getStart() != null ? String.format("%f", edit.getStart()) : "-1";
            String end = edit.getEnd() != null ? String.format("%f", edit.getEnd()) : "-1";
            switch (file.getResourceType()) {

                case AUDIO:
                    command = String.format("python %s %s %s %s %s %s",
                            pythonSrcriptPath, getTemporaryFilePath(file.getExtension(), key), start, end, bucketName, profileName);
                    break;
                case VIDEO:
                    command = String.format("python %s %s %s %s %b %s %s",
                            pythonSrcriptPath, getTemporaryFilePath(file.getExtension(), key), start, end, edit.isWithAudio(), bucketName, profileName);
                    break;
                default:
                    throw new RuntimeException("Unknown media type");
            }

           return executePythonScripts(command);
    }
}
