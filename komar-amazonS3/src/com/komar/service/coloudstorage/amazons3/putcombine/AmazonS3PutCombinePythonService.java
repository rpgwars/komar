package com.komar.service.coloudstorage.amazons3.putcombine;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.komar.domain.cloudstorage.resource.transfer.put.PutResultTO;
import com.komar.domain.resource.transfer.ClipCombination;
import com.komar.service.cloudstorage.put.PutCombineService;
import com.komar.service.cloudstorage.put.PutException;
import com.komar.service.coloudstorage.amazons3.put.AmazonS3AbstractPutPythonService;
import com.komar.service.coloudstorage.amazons3.put.AmazonS3PutResultTO;

@Service
public class AmazonS3PutCombinePythonService extends AmazonS3AbstractPutPythonService implements PutCombineService{
	
	private static final String defaultCombinationMimeType = "video/mp4";
	
	private ObjectMapper objectMapper = new ObjectMapper();
	
	@Override
	public PutResultTO putCombine(ClipCombination clipCombination) throws PutException {
		String key = keyGenerator.getKey();
		try {
			String clipCombinationJson = objectMapper.writeValueAsString(clipCombination);
			saveFile(clipCombinationJson.getBytes(), key);
	        String command = String.format("python %s %s %s %s %s",
	                pythonSrcriptPath, key, tmpFilesFolderPath, bucketName, profileName);
	        String link = executePythonScripts(command);
	        return new AmazonS3PutResultTO(bucketName, key, link, defaultCombinationMimeType);
		} catch (JsonProcessingException e) {
			throw new PutException();
		}
	}
}
