package com.komar.service.coloudstorage.amazons3.put;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Logger;
import java.util.logging.Level;

import org.springframework.beans.factory.annotation.Autowired;

import com.komar.domain.resource.transfer.UploadedFile;
import com.komar.service.cloudstorage.put.PutException;
import com.komar.service.coloudstorage.amazons3.AmazonS3KeyGenerator;

public abstract class AmazonS3AbstractPutPythonService {
	
    protected String pythonSrcriptPath;
    protected String tmpFilesFolderPath;
	protected String bucketName; 
	protected String profileName;
	
	private final static Logger logger = Logger.getLogger(AmazonS3AbstractPutPythonService.class.getName());
    
    @Autowired
    protected AmazonS3KeyGenerator keyGenerator;
    
    protected void saveFile(UploadedFile file, String key) throws PutException {
        File ordinaryFile = new File(getTemporaryFilePath(file.getExtension(), key));
        try {
			saveFile(ordinaryFile, file.getFile().getBytes());
		} catch (IOException e) {
			throw new PutException();
		}
    }
    
    protected void saveFile(byte[] content, String key) throws PutException{
    	File ordinaryFile = new File(getTemporaryFilePath("", key));
    	saveFile(ordinaryFile, content);
    }
    
    private void saveFile(File ordinaryFile, byte[] content) throws PutException{
        try {
            ordinaryFile.createNewFile();
            FileOutputStream fileOutputStream = new FileOutputStream(ordinaryFile);
            fileOutputStream.write(content);
            fileOutputStream.close();
        } catch (IOException e) {
            throw new PutException();
        }
    }
    
    
    protected String executePythonScripts(String command) throws PutException {
        try {
            Process p = Runtime.getRuntime().exec(command);
            BufferedReader stdInput = new BufferedReader(new
                    InputStreamReader(p.getInputStream()));

            String line;
            String link = null;
            while ((line = stdInput.readLine()) != null) {
                System.out.println(line);
                link = line;
            }
            
            if(p.waitFor() != 0){
                BufferedReader errInput = new BufferedReader(new
                        InputStreamReader(p.getErrorStream()));
            	logger.log(Level.SEVERE, errInput.lines().reduce("", String::concat));
                throw new PutException();
            }
             
            return link;
        } catch (IOException e) {
            throw new PutException();
        }
        catch (InterruptedException e) {
            throw new PutException();
        }
    }

    protected String getTemporaryFilePath(String extension, String key) {
        return tmpFilesFolderPath + key + extension;
    } 
    
    public void setPythonSrcriptPath(String pythonSrcriptPath) {
        this.pythonSrcriptPath = pythonSrcriptPath;
    }

    public void setTmpFilesFolderPath(String tmpFilesFolderPath) {
        this.tmpFilesFolderPath = tmpFilesFolderPath;
    }
    
    public void setKeyGenerator(AmazonS3KeyGenerator keyGenerator) {
        this.keyGenerator = keyGenerator;
    }
	public void setBucketName(String bucketName){
		this.bucketName = bucketName;
	}
	
	public void setProfileName(String profileName){
		this.profileName = profileName;
	}
}
