package com.komar.service.coloudstorage.amazons3.putedit;

import org.springframework.stereotype.Service;

import com.komar.domain.cloudstorage.resource.transfer.put.PutResultTO;
import com.komar.domain.resource.transfer.Edit;
import com.komar.service.cloudstorage.put.PutEditService;
import com.komar.service.cloudstorage.put.PutException;
import com.komar.service.coloudstorage.amazons3.put.AmazonS3AbstractPutPythonService;
import com.komar.service.coloudstorage.amazons3.put.AmazonS3PutResultTO;

@Service
public class AmazonS3PutEditPythonYoutubeService extends AmazonS3AbstractPutPythonService implements PutEditService<String>{

	@Override
	public PutResultTO putEdit(String url, Edit edit) throws PutException {
		String key = keyGenerator.getKey();
		String link = executePythonScripts(key, url, edit);
		return new AmazonS3PutResultTO(bucketName, key, link, null);
	}
	
	private String executePythonScripts(String key, String url, Edit edit) throws PutException{
        String start = edit.getStart() != null ? String.format("%f", edit.getStart()) : "-1";
        String end = edit.getEnd() != null ? String.format("%f", edit.getEnd()) : "-1";
		String command = String.format("python %s youtube %s %s %s %s %s %b %b %s %s",
                pythonSrcriptPath, url, key, tmpFilesFolderPath, start, end, edit.isWithAudio(), edit.isExtractAudio(), bucketName, profileName);
		return executePythonScripts(command);
	}

}
