import youtube_dl
from glob import glob

filePath = ''

class MyLogger(object):
    def debug(self, msg):
        pass

    def warning(self, msg):
        pass

    def error(self, msg):
        pass

def my_hook(d):
	pass
    
def getYouTubeVideoClip(youTubeLink, key, fileFolder, extractAudio):
    fileTemplate = fileFolder + key + '.%(ext)s'

    ydl_opts = {
 	'max_filesize' : 50000000,
	'outtmpl': fileTemplate,
	'logger': MyLogger(),
        'progress_hooks': [my_hook],
    }

    if(extractAudio):
        ydl_opts['postprocessors'] = [{
            'key': 'FFmpegExtractAudio'
        }]

    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
	ydl.download([youTubeLink])

    filePaths = glob(fileFolder + key + ".*")
    if len(filePaths) == 1:
	filePath = filePaths[0]
    
    return filePath

#print getYouTubeVideoClip('https://www.youtube.com/watch?v=69khlS8RzQ8', 'yKey', '/home/damian/komar/python/', True)
