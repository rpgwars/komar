package com.komar.domain.resource.transfer;

import java.util.List;

public class ClipCombination {

    private String name;
    private List<ClipTO> videoClips;
    private List<List<ClipTO>> selectedAudioClips;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ClipTO> getVideoClips() {
        return videoClips;
    }

    public void setVideoClips(List<ClipTO> videoClips) {
        this.videoClips = videoClips;
    }

    public List<List<ClipTO>> getSelectedAudioClips() {
        return selectedAudioClips;
    }

    public void setSelectedAudioClips(List<List<ClipTO>> selectedAudioClips) {
        this.selectedAudioClips = selectedAudioClips;
    }
}
