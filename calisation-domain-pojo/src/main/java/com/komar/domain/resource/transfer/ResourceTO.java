package com.komar.domain.resource.transfer;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class ResourceTO {

    private String name;
    private String dateTime;
    private String retrievalLink;
    private String authorName;

    private List<ResourceLinkTO> resourceLinkTOList;
    
    public ResourceTO(String name, LocalDateTime dateTime, String retrievalLink, String authorName){
    	this.name = name; 
    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    	this.dateTime = dateTime.format(formatter); 
    	this.retrievalLink = retrievalLink; 
    	this.authorName = authorName;
    }
    
    public String getResourceKey(){
    	String[] splitted = retrievalLink.split("/");
    	return splitted[splitted.length - 1].split("\\.")[0];
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getRetrievalLink() {
        return retrievalLink;
    }

    public void setRetrievalLink(String retrievalLink) {
        this.retrievalLink = retrievalLink;
    }

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}
}
