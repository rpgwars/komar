package com.calisation.controller.validator;

import com.komar.domain.resource.transfer.ClipCombination;
import com.komar.domain.resource.transfer.ClipTO;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Iterator;
import java.util.List;

public class ClipCombinationValidator implements Validator{

    String clipsCombinationError = "validation.clipsCombination.error";

    @Override
    public boolean supports(Class<?> cls) {
        return ClipCombination.class.isAssignableFrom(cls);
    }

    @Override
    public void validate(Object object, Errors errors) {
        if(object instanceof ClipCombination){
            ClipCombination clipCombination = (ClipCombination) object;
            List<List<ClipTO>> selectedAudioClips = clipCombination.getSelectedAudioClips();
            List<ClipTO> videoClips = clipCombination.getVideoClips();
            if(selectedAudioClips.size() != videoClips.size()) {
                errors.reject(clipsCombinationError);
                return;
            }

            if(videoClips.isEmpty()){
                errors.reject(clipsCombinationError);
                return;
            }

            Iterator<ClipTO> videoIterator = videoClips.iterator();
            Iterator<List<ClipTO>> selectedAudioIterator = selectedAudioClips.iterator();

            while(videoIterator.hasNext() && selectedAudioIterator.hasNext()){
                ClipTO video = videoIterator.next();
                List<ClipTO> audio = selectedAudioIterator.next();
                if(video.isWithAudio() && audio.size() > 0) {
                    errors.reject(clipsCombinationError);
                    return;
                }
                
                if(video.getVolume() < 0 || video.getVolume() > 1){
                    errors.reject(clipsCombinationError);
                    return;
                }
                
                if(audio.stream().anyMatch(audioTrack -> audioTrack.getVolume() < 0 || audioTrack.getVolume() > 1)){
                    errors.reject(clipsCombinationError);
                    return;
                }
            }
        }
        else
            throw new RuntimeException();
    }
}
