package com.calisation.controller;

import com.calisation.controller.validator.ClipCombinationValidator;
import com.calisation.controller.validator.UploadedFileValidator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.komar.domain.account.exception.AccountNotFound;
import com.komar.domain.cloudstorage.resource.transfer.ResourceType;
import com.komar.domain.cloudstorage.resource.transfer.put.PutResultTO;
import com.komar.domain.resource.transfer.ClipCombination;
import com.komar.domain.resource.transfer.ClipTO;
import com.komar.domain.resource.transfer.Edit;
import com.komar.domain.resource.transfer.UploadedFile;
import com.komar.service.application.ClipService;
import com.komar.service.application.UploadService;
import com.komar.service.application.exception.MaximalClipsNumberExceeded;
import com.komar.service.cloudstorage.delete.DeleteService;
import com.komar.service.cloudstorage.put.PutCombineService;
import com.komar.service.cloudstorage.put.PutEditService;
import com.komar.service.cloudstorage.put.PutException;
import com.komar.service.cloudstorage.put.PutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.beans.PropertyEditorSupport;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Controller
public class UploadController {

    @Autowired
    private UploadService uploadService;

    @Autowired
    private PutCombineService putCombineService;

    @Autowired
    @Qualifier("putEditService")
    private PutEditService<UploadedFile> putEditService;
    
    @Autowired
    @Qualifier("putEditYoutubeService")
    private PutEditService<String> putEditYoutubeService;

    @Autowired
    private DeleteService deleteService;

    @Autowired
    private ClipService clipService;

    @Autowired
    private UploadedFileValidator uploadedFileValidator;

    @Autowired
    private ClipCombinationValidator clipCombinationvalidator;

    private ObjectMapper objectMapper = new ObjectMapper();
    {
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    private static final String myResourcesView = "myResources";

    private static final String resourceModel = "resource";
    private static final String audioClipsModel = "audioClips";
    private static final String videoClipsModel = "videoClips";
    private static final String clipCombinationModel = "clipCombination";

    private final static Logger logger = Logger.getLogger(UploadController.class.getName());

    @RequestMapping("/myResources")
    public String sandbox(Map<String, Object> map) {
        map.put(resourceModel, new UploadedFile());
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        List<ClipTO> clips = clipService.getClips(login);
        try {
            map.put(audioClipsModel, objectMapper.writeValueAsString(clips.stream().filter(clip -> ResourceType.AUDIO.equals(clip.getResourceType())).collect(Collectors.toList())));
            map.put(videoClipsModel, objectMapper.writeValueAsString(clips.stream().filter(clip -> ResourceType.VIDEO.equals(clip.getResourceType())).collect(Collectors.toList())));
            map.put(clipCombinationModel, new ClipCombination());
        } catch (JsonProcessingException e) {
            logger.log(Level.WARNING, "Unable to parse to json objects", e);
        }
        return myResourcesView;
    }

    @RequestMapping(value = "/myResources/addClip", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<String> addClip(@RequestParam(required = false) Float start, @RequestParam(required = false) Float end,
                                                        @RequestParam(required = false, defaultValue = "true") Boolean withAudio, @RequestParam String name,
                                                        @RequestParam("file") MultipartFile clip) {
        Edit edit = new Edit();
        edit.setWithAudio(withAudio);
        edit.setStart(start);
        edit.setEnd(end);
        Optional<UploadedFile> file = uploadedFileValidator.validateClip(edit, clip);
        if(!file.isPresent() || !uploadedFileValidator.validateFileName(name)){
            return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
        }
        UploadedFile uploadedFile = file.get();
        uploadedFile.setName(name);
        String login = SecurityContextHolder.getContext().getAuthentication().getName();

        try {
            clipService.isAddingClipPossible(login);
            PutResultTO putResultTO = putEditService.putEdit(uploadedFile, edit);
            List<ClipTO> clipTOs = clipService.saveClip(putResultTO, login, name, uploadedFile.getResourceType(), withAudio);
            return new ResponseEntity<String>(objectMapper.writeValueAsString(clipTOs), HttpStatus.CREATED);
        } catch (PutException e) {
            logger.log(Level.WARNING, "Unable to complete add_clip request", e);
            return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (MaximalClipsNumberExceeded maximalClipsNumberExceeded) {
            return new ResponseEntity<String>(HttpStatus.FORBIDDEN);
        } catch (AccountNotFound accountNotFound) {
            return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
        } catch (JsonProcessingException e) {
            logger.log(Level.WARNING, "Unable to parse to json objects", e);
            return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    @RequestMapping(value = "/myResources/addYoutubeClip", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<String> addYoutubeClip(@RequestParam(required = false) Float start, @RequestParam(required = false) Float end,
                                                        @RequestParam(required = false, defaultValue = "true") Boolean withAudio,
                                                        @RequestParam(required = false, defaultValue = "false") Boolean extractAudio,
                                                        @RequestParam String name, @RequestParam String youtubeUrl) {
        Edit edit = new Edit(); 
        edit.setWithAudio(withAudio);
        edit.setStart(start);
        edit.setEnd(end);
        edit.setExtractAudio(extractAudio);

        if(!uploadedFileValidator.validateFileName(name) || !uploadedFileValidator.validateEdit(edit)){
            return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
        }
              
        String login = SecurityContextHolder.getContext().getAuthentication().getName();

        try {
            clipService.isAddingClipPossible(login);
            PutResultTO putResultTO = putEditYoutubeService.putEdit(youtubeUrl, edit);
            List<ClipTO> clipTOs = clipService.saveClip(putResultTO, login, name, edit.isExtractAudio() ? ResourceType.AUDIO : ResourceType.VIDEO, withAudio);
            return new ResponseEntity<String>(objectMapper.writeValueAsString(clipTOs), HttpStatus.CREATED);
        } catch (PutException e) {
            logger.log(Level.WARNING, "Unable to complete add youtube clip request", e);
            return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (MaximalClipsNumberExceeded maximalClipsNumberExceeded) {
            return new ResponseEntity<String>(HttpStatus.FORBIDDEN);
        } catch (AccountNotFound accountNotFound) {
            return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
        } catch (JsonProcessingException e) {
            logger.log(Level.WARNING, "Unable to parse to json objects", e);
            return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/myResources/deleteClip", method = RequestMethod.DELETE)
    public @ResponseBody ResponseEntity<String> deleteClip(@RequestParam String url) {
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        clipService.deleteClip(login, url);
        String[] splittedUrl = url.split("/");
        deleteService.delete(splittedUrl[splittedUrl.length - 1], null);
        try {
            return new ResponseEntity<String>(objectMapper.writeValueAsString(clipService.getClips(login)), HttpStatus.OK);
        } catch (JsonProcessingException e) {
            logger.log(Level.WARNING, "Unable to parse to json objects", e);
            return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @RequestMapping(value = "/uploadResource", method = RequestMethod.POST)
    public String uploadFileHandler(@ModelAttribute(clipCombinationModel) @Valid ClipCombination clipCombination,
                                          BindingResult bindingResult) {

        String failureUrl = "redirect: " + myResourcesView + "?successful=false";
        if(bindingResult.hasErrors())
            return failureUrl;

    	try {
            PutResultTO putResultTO = putCombineService.putCombine(clipCombination);
            uploadService.saveResource(putResultTO, SecurityContextHolder.getContext().getAuthentication().getName(), clipCombination.getName());
        } catch (PutException putException) {
            logger.log(Level.WARNING, "Unable to complete combine clips request - unsuccessful put", putException);
            return failureUrl;
		} catch (AccountNotFound accountNotFound) {
            logger.log(Level.WARNING, "Unable to complete combine clips request - username not found", accountNotFound);
            return failureUrl;
        }
        return "redirect: " + myResourcesView + "?successful=true";
    }

    @InitBinder(clipCombinationModel)
    protected void initBinder(WebDataBinder binder) throws Exception {
        binder.registerCustomEditor(List.class, "selectedAudioClips", new PropertyEditorSupport() {
            @Override
            public void setAsText(String selectedAudioclipsJson) {
                List<ClipTO> selectedAudioClips = null;
                try {
                    selectedAudioClips = objectMapper.readValue(selectedAudioclipsJson, new TypeReference<List<List<ClipTO>>>(){});
                    setValue(selectedAudioClips);
                } catch (IOException e) {
                    setValue(Collections.<List<ClipTO>>emptyList());
                }
            }
        });

        binder.registerCustomEditor(List.class, "videoClips", new PropertyEditorSupport() {
            @Override
            public void setAsText(String videoClipsJson) {
                List<ClipTO> videoClips = null;
                try {
                    videoClips = objectMapper.readValue(videoClipsJson, new TypeReference<List<ClipTO>>(){});
                    setValue(videoClips);
                } catch (IOException e) {
                    setValue(Collections.<ClipTO>emptyList());
                }

            }
        });

        binder.setValidator(clipCombinationvalidator);
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public void handle(HttpMessageNotReadableException e) {
        System.out.println("BAD REQUEST");
    }

    @InitBinder("resource")
    protected void initAccountBinder(WebDataBinder binder) {
        binder.setValidator(uploadedFileValidator);
    }
}
