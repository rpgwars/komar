package com.calisation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.komar.service.application.ResourceRetrievalService;

@Controller
public class MainViewController {
	
	private static final String leftmostPageNrModel = "leftmostPageNr";
	private static final String rightmostPageNrModel = "rightmostPageNr";
	private static final String resourcesModel = "resources";
	private static final String resourceModel = "resource";
	private static final int pagesNextToCurrentNr = 5;
	
	private static final String mainViewView = "mainView";
	private static final String singleResourceView = "singleResource";
	
	@Autowired
	private ResourceRetrievalService resourceRetrievalService;

    @RequestMapping("/view/{pageNr}")
    public ModelAndView showResources(@PathVariable Integer pageNr) {
    	ModelAndView modelAndView = new ModelAndView();
    	preparePageNumbers(modelAndView, pageNr);
    	modelAndView.addObject(resourcesModel, resourceRetrievalService.getResources(pageNr));
    	modelAndView.setViewName(mainViewView);
    	return modelAndView;
    }
    
    @RequestMapping("/viewClip/{key}")
    public ModelAndView showResource(@PathVariable String key) {
    	if(key == null || key.length() < 26)
    		return new ModelAndView("redirect:/view/0");
    	ModelAndView modelAndView = new ModelAndView();
    	modelAndView.addObject(resourceModel, resourceRetrievalService.getResource(key));
    	modelAndView.setViewName(singleResourceView);
    	return modelAndView;
    }
    
    
    private void preparePageNumbers(ModelAndView modelAndView, Integer pageNr){
    	int pagesNr = resourceRetrievalService.getPagesNr();
    	int leftmostPageNr = pageNr - pagesNextToCurrentNr < 1 ? 0 : pageNr - pagesNextToCurrentNr;
    	int rightmostPageNr = pageNr + pagesNextToCurrentNr > pagesNr - 2 ? pagesNr - 1 : pageNr + pagesNextToCurrentNr;
    	if(rightmostPageNr < 0)
    		rightmostPageNr = 0;
    	modelAndView.addObject(leftmostPageNrModel, leftmostPageNr);
    	modelAndView.addObject(rightmostPageNrModel, rightmostPageNr);
    }
	
}
