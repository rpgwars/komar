<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div>
	<div>
		<span> Name: ${resource.name}</span>
		<span> Author: ${resource.authorName}</span>
		<span> date: ${resource.dateTime}</span>
	</div>
	<video controls style="display: block; margin-left: auto; margin-right: auto;">
		<source src="${resource.retrievalLink}"/>
	</video>
</div>