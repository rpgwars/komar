<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v2.4";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div>
	<c:forEach var="resource" items="${resources}">
		<div>
			<span> Name: ${resource.name}</span>
			<span> Author: ${resource.authorName}</span>
			<span> date: ${resource.dateTime}</span>
			<button onclick="openClip('${resource.getResourceKey()}')">Otworz</button>
		</div>
		<video controls style="display: block; margin-left: auto; margin-right: auto;">
			<source src="${resource.retrievalLink}"/>
		</video>
		<div class="fb-like" data-href="<%=request.getScheme() + "://" + request.getServerName()%>/viewClip/${resource.getResourceKey()}" data-layout="button_count" 
			data-action="like" data-show-faces="true" data-share="true"></div>
	</c:forEach>
            
	<c:choose>
		<c:when test="${leftmostPageNr != rightmostPageNr}">
			<c:forEach var="pageNr" begin="${leftmostPageNr}" end="${rightmostPageNr}">
				<a href="/view/${pageNr}">${pageNr+1}</a>
			</c:forEach>		
		</c:when>
		<c:otherwise>
		
		</c:otherwise>
	</c:choose>   
</div>

<script src="/js/resourcesView.js"></script>