# README #

The code backing up mytopplays.com service (inactive) for uploading players favourite video clips and creating clip compilations.

Subprojects:

* calisation: top level maven project
* calisation-domain: db entities
* calisation-domain-pojo: plain old java objects i.e. mapped domain objects
* calisation-repository: access and modification of the domain objects
* calisation-service: business operations (for instance clip uploading)
* calisation-weapp: js, html, REST endpoints and servlet
* komar-amazonS3: uploading and accessing resources on S3 buckets